package unicode.com.gitrepos;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.net.URL;

import unicode.com.gitrepos.utils.NetworkUtils;

    public class MainActivity extends AppCompatActivity {

        TextView textView;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            textView = (TextView)findViewById(R.id.tv_texto_inicial);


        }


        //Metodo para clicar nos item de menu
        @Override
        public boolean onOptionsItemSelected(MenuItem item) {

            //Testa o item id se é igual com o id do main.xml
            if (item.getItemId() == R.id.id_item1){
                URL url = NetworkUtils.buildUrl("android");
                textView.setText(url.toString());
            }else {
                //senão seta um valor em branco ou seja limpa a tela
                textView.setText("");
            }

            //textView.setText(item.getTitle().toString());

            return super.onOptionsItemSelected(item);
        }



        //Metodo para Criar As opções de Menu
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.main, menu);
            return super.onCreateOptionsMenu(menu);

        }


    }



